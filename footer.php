	</main> <!-- .site-content -->

	<footer class="site-footer grid">
		<div class="site-logo">
			<a href="/">
				<img src="<?php bloginfo('template_directory'); ?>/images/heavy-goods-logo-white.svg" alt="Heavy Goods" />
			</a>
		</div>

	</footer>

<?php wp_footer(); ?>

</div> <!-- .site -->
</body>
</html>