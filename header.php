<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">
	
	<header class="site-header grid">
		<div class="hamburger">
			<a href="#" class="bun nav-trigger">
				<div class="patty"></div>
			</a>
		</div>

		<div class="site-logo">
			<a href="/">
				<img src="<?php bloginfo('template_directory'); ?>/images/heavy-goods-logo.svg" alt="Heavy Goods" />
			</a>
		</div>

		<div class="utility-links">
			<a href="#" class="utility-link account">
				<img src="<?php bloginfo('template_directory'); ?>/images/icon-account.svg" alt="Account" />
			</a>		

			<a href="#" class="utility-link search">
				<img src="<?php bloginfo('template_directory'); ?>/images/icon-search.svg" alt="Search" />
			</a>		

			<a href="#" class="utility-link cart">
				<img src="<?php bloginfo('template_directory'); ?>/images/icon-cart.svg" alt="Cart" />
			</a>
		</div>
	</header>

	<nav class="site-nav">
		<div class="site-nav-wrapper">

			<ul>
				<li class="first-level"><a href="#" class="first-level-link">Meal Kits</a></li>
				<li class="first-level">
					<a href="#" class="first-level-link toggle"><span>Market Goods</span></a>
					<ul class="second-level">
						<li><a href="#" class="second-level-link">Heat & Eat</a></li>
						<li><a href="#" class="second-level-link">Mexican Inspired</a></li>
						<li><a href="#" class="second-level-link">Italian Inspired</a></li>
						<li><a href="#" class="second-level-link">Meats</a></li>
						<li><a href="#" class="second-level-link">Sauces & Marinades</a></li>
						<li><a href="#" class="second-level-link">Salads & Sides</a></li>
						<li><a href="#" class="second-level-link">Cheese & Charcuterie</a></li>
						<li><a href="#" class="second-level-link">Sweets & Treats</a></li>
					</ul>
				</li>
				<li class="first-level">
					<a href="#" class="first-level-link toggle"><span>Home Goods</span></a>
					<ul class="second-level">
						<li><a href="#" class="second-level-link">Table Top</a></li>
						<li><a href="#" class="second-level-link">Candles</a></li>
						<li><a href="#" class="second-level-link">Textiles</a></li>
						<li><a href="#" class="second-level-link">Gifts</a></li>
					</ul>
				</li>
				<li class="first-level"><a href="#" class="first-level-link">Bar</a></li>
				<li class="first-level"><a href="#" class="first-level-link">Cellar</a></li>
				<li class="first-level"><a href="#" class="first-level-link">Classes</a></li>
			</ul>

			<div class="note">
				<p>For pick-up, delivery, and catering from any of our restaurants, visit:</p>

				<div class="cta">
					<a href="https://heavyathome.com/"><img src="<?php bloginfo('template_directory'); ?>/images/heavy-at-home-logo-white.svg" alt="Heavy Goods" /></a>
				</div>
			</div>		

		</div>
	</nav>

	<main class="site-content">	
