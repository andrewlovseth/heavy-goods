(function ($, window, document, undefined) {


	$(document).ready(function($) {

		// Nav Trigger
		$('.nav-trigger').click(function(){
			$('body').toggleClass('nav-overlay-open');
			return false;
		});

		$('.first-level-link.toggle').click(function(){
			$(this).toggleClass('active');
			$(this).siblings('.second-level').toggleClass('active');

			return false;
		});
			
	});


	$(document).mouseup(function(e) {

		var site_menu = $('.site-nav, .nav-trigger');	
		if (!site_menu.is(e.target) && site_menu.has(e.target).length === 0) {
			$('body').removeClass('nav-overlay-open');
		}

	});


	$(document).keyup(function(e) {		

		if (e.keyCode == 27) {
			$('body').removeClass('nav-overlay-open');
		}

	});


})(jQuery, window, document);