<?php get_header(); ?>

    <section class="hero grid">
        <div class="photo">
            <div class="content">
                <img src="<?php bloginfo('template_directory'); ?>/images/home-hero.jpg" alt="Hero" />
            </div>
        </div>

        <div class="info">
            <div class="headline">
                <h1>your favorite craft cocktails delivered to your door</h1>
            </div>
        </div>        
    </section>

    <section class="intro grid">
        <div class="headline">
            <h2>Hello, there!</h2>
        </div>

        <div class="copy p1">
            <p>Welcome to Heavy Goods. We deliver freshly prepared food items inspired by our Heavy Food Group family of restaurants right to your home. From meal kits to market goods, we have your answer to “what’s for dinner?” Complete your meal with a drink selection from our bar or cellar. Set the mood with our curated locally-made home goods. </p>
        </div>

        <div class="tagline headline section-header">
            <h3>Eat Good & Drink Well</h3>
        </div>

        <div class="restaurants">
            <div class="headline">
                <h4>Inspired by:</h4>
            </div>

            <div class="links">
                <a href="#">Purple</a>
                <a href="#">Barrio</a>
                <a href="#">The Commons</a>
                <a href="#">Fiasco</a>
                <a href="#">Meet the Moon</a>
                <a href="#">Pablo y Pablo</a>
            </div>       
        </div>
    </section>

    <section class="features features-1 grid">
        <div class="two-col-features">

            <div class="feature">
                <a href="#">
                    <div class="photo">
                        <div class="content">
                            <img src="<?php bloginfo('template_directory'); ?>/images/FPO-home-market.jpg" alt="" />
                        </div>
                    </div>

                    <div class="headline">
                        <h2>Market</h2>
                    </div>
                </a>
            </div>

            <div class="feature">
                <a href="#">
                    <div class="photo">
                        <div class="content">
                            <img src="<?php bloginfo('template_directory'); ?>/images/FPO-home-drinks.jpg" alt="" />
                        </div>
                    </div>

                    <div class="headline">
                        <h2>Home Goods</h2>
                    </div>
                </a>
            </div>

        </div>
    </section>

    <section class="side-by-side side-by-side-1 grid">
        <div class="two-col-info">
            <div class="photo">
                <div class="content">
                    <img src="<?php bloginfo('template_directory'); ?>/images/FPO-home-meal-kits.jpg" alt="" />
                </div>
            </div>

            <div class="info">
                <div class="info-wrapper">
                    <div class="headline section-header">
                        <h3>Meal Kits</h3>
                    </div>

                    <div class="copy p2">
                        <p>Thoughtfully packaged three-course dinners for two. Complete at home in a few easy steps.</p>
                    </div>                
                </div>
            </div>
        
        </div>
    </section>

    <section class="features features-2 grid">
        <div class="two-col-features">

            <div class="feature">
                <a href="#">
                    <div class="photo">
                        <div class="content">
                            <img src="<?php bloginfo('template_directory'); ?>/images/FPO-home-drinks.jpg" alt="" />
                        </div>
                    </div>

                    <div class="headline">
                        <h2>Bar</h2>
                    </div>
                </a>
            </div>

            <div class="feature">
                <a href="#">
                    <div class="photo">
                        <div class="content">
                            <img src="<?php bloginfo('template_directory'); ?>/images/FPO-home-drinks.jpg" alt="" />
                        </div>
                    </div>

                    <div class="headline">
                        <h2>Cellar</h2>
                    </div>
                </a>
            </div>

        </div>
    </section>

    <section class="products popular grid">
        <div class="headline section-header align-center">
            <h3>Popular Goods</h3>
        </div>

        <div class="three-col-products">

            <?php
                for($i = 1; $i<=6; $i++) {
                    get_template_part('template-parts/FPO-product');
                }
            ?>

        </div>

    </section>

    <section class="newsletter grid">
        <div class="headline">
            <h2>Sign up for our Heavy Goods newsletter to stay up to date on new items, classes, and sales.</h2>
        </div>

        <div class="cta sienna align-center">
            <a href="#" class="btn">Sign up for good stuff</a>
        </div>

    </section>


<?php get_footer(); ?>