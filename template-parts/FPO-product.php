<div class="product">
    <a href="#">
        <div class="photo">
            <div class="content">
                <img src="<?php bloginfo('template_directory'); ?>/images/FPO-product.jpg" alt="" />
            </div>
        </div>

        <div class="details">
            <div class="name">
                <h3>Cranberry Mustard</h3>
            </div>

            <div class="price">
                <span>$64</span>
            </div>
        </div>
    </a>
</div>